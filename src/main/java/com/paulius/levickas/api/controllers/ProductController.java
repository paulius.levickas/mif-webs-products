package com.paulius.levickas.api.controllers;

import com.paulius.levickas.api.controllers.beans.ProductBean;
import com.paulius.levickas.api.models.Product;
import com.paulius.levickas.api.services.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class ProductController {

    private final ProductService productService;

    public ProductController(final ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@RequestBody final Product product) {
        return productService.createProduct(product);
    }

    @GetMapping("/products/{productId}")
    public Product findProductById(@PathVariable final Long productId) {
        return productService.findProductById(productId);
    }

    @PutMapping("/products/{productId}")
    public Product updateProduct(
            @PathVariable final Long productId,
            @RequestBody final ProductBean productBean) {
        return productService.updateProduct(productId, productBean);
    }

    @DeleteMapping("/products/{productId}")
    public ResponseEntity deleteProduct(@PathVariable final Long productId) {
        productService.deleteProduct(productId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
