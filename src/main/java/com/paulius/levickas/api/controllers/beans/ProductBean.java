package com.paulius.levickas.api.controllers.beans;

import com.paulius.levickas.api.models.Attributes;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class ProductBean implements Serializable {
    private String name;
    private Double price;
    private int inStock;
    private Set<Attributes> attributes;
}
