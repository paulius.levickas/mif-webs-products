package com.paulius.levickas.api.services;

import com.fasterxml.jackson.databind.util.BeanUtil;
import com.paulius.levickas.api.controllers.beans.ProductBean;
import com.paulius.levickas.api.exceptions.ProductDuplicateException;
import com.paulius.levickas.api.exceptions.ProductNotFoundException;
import com.paulius.levickas.api.models.Product;
import com.paulius.levickas.api.repositories.ProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public ResponseEntity<Product> createProduct(final Product product) {
        Optional<Product> existingProduct = productRepository.findByName(product.getName());
        if (existingProduct.isPresent()) {
            throw new ProductDuplicateException();
        }
        Product dbProduct = productRepository.save(product);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("location",
                "/products/" + dbProduct.getId());
        try {
            return ResponseEntity.created(new URI("/products/" + dbProduct.getId())).headers(responseHeaders).body(product);
        } catch (Exception e) {
            throw new RuntimeException("Failed creating response");
        }
    }

    public Product findProductById(final Long productId) {
        return productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
    }

    public Product updateProduct(final Long productId, final ProductBean productBean) {
        Product product = productRepository.findById(productId).orElseThrow(ProductNotFoundException::new);
        copyNonNullProperties(productBean, product);
        return productRepository.save(product);
    }

    public ResponseEntity<String> deleteProduct(final Long productId) {
        if(!productRepository.existsById(productId)) {
            throw new ProductNotFoundException();
        }
        productRepository.deleteById(productId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public void copyNonNullProperties(Object source, Object destination){
        BeanUtils.copyProperties(source, destination,
                getNullPropertyNames(source));
    }

    private String[] getNullPropertyNames (Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for(java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}
