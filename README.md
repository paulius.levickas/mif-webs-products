# Product storage

## Launch the app

Follow steps below in order to launch the application:

1. ```mvn clean package``` - to build the application.
2. ```docker build -t item-storage .``` - to build the image.
3. ```docker image ls``` - to list all your docker images and verify if the newly built one exists.
4. ```docker run -d -p 5000:8080 --name item-storage item-storage``` - to run the application on port ```5000```

## How to use the api

### GET

```aidl
curl --location --request GET 'http://localhost:5000/products' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'

```

```
curl --location --request GET 'http://localhost:5000/products/1' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
```

### DELETE

```aidl
curl --location --request DELETE 'http://localhost:5000/products/4' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
```

### POST

```aidl
curl --location --request POST 'http://localhost:5000/products' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'Content-Type: application/json' \
--data-raw '{
	"name": "Lova",
	"inStock": 34,
	"price": 54.33,
	"attributes": [
		{
		"name": "aukstis",
		"value": "32cm"
		},
		{
		"name": "plotis",
		"value": "160cm"
		}
		]
}'
```

### PUT

```aidl
curl --location --request PUT 'http://localhost:5000/products/1' \
  -H 'accept: application/json' \
  -H 'cache-control: no-cache' \
  -H 'Content-Type: application/json' \
--data-raw '{
    "price": 99.55
}'
```

author Paulius Levickas